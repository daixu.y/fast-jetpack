package com.aisier.architecture.base

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.aisier.architecture.util.toast
import com.jeremyliao.liveeventbus.LiveEventBus

/**
 * <pre>
 * author : wutao
 * e-mail : 670831931@qq.com
 * time   : 2021/6/18
 * desc   : 去掉类上面的泛型，因为反射会影响性能。并且优先选择组合而不是继承。
 * version: 1.3
</pre> *
 */
abstract class BaseActivity<VB : ViewDataBinding> : AppCompatActivity(), IUiView {

    protected var binding: VB? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayoutId() != 0) {
            binding = DataBindingUtil.setContentView(this, getLayoutId())
        }
        binding?.lifecycleOwner = this
        setContentView(binding?.root)
        init()
        observeToast()
    }

    private fun observeToast() {
        LiveEventBus.get<String>(SHOW_TOAST).observe(this) {
            toast(it)
        }
    }

    protected abstract fun init()

    protected abstract fun getLayoutId(): Int

    private var progressDialog: ProgressDialog? = null

    override fun showLoading() {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog?.show()
    }

    override fun dismissLoading() {
        progressDialog?.takeIf { it.isShowing }?.dismiss()
    }

    companion object {
        const val SHOW_TOAST = "show_toast"
    }
}