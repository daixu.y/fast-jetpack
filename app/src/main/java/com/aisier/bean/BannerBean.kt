package com.aisier.bean

data class BannerBean(
    val desc: String,
    val image: String,
    val jump: String
)