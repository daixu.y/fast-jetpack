package com.aisier.bean

data class UpdateInventoryBean(
    val id: String,
    val skus: List<Sku>
)

data class Sku(
    val inventory: String,
    val skuid: String
)