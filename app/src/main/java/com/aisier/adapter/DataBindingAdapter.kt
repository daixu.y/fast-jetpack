package com.aisier.adapter

import com.aisier.R
import com.aisier.bean.BannerBean
import com.aisier.databinding.ItemBannerBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder

/**
 * @author: limuyang
 * @date: 2019-12-05
 * @Description: DataBinding Adapter
 */
class DataBindingAdapter : BaseQuickAdapter<BannerBean, BaseDataBindingHolder<ItemBannerBinding>> {
    constructor() : super(R.layout.item_banner) {

    }

    constructor(list: List<BannerBean>) : super(R.layout.item_banner) {

    }

    constructor(layoutResId: Int, list: MutableList<BannerBean>) : super(layoutResId, list) {

    }

    override fun convert(holder: BaseDataBindingHolder<ItemBannerBinding>, item: BannerBean) {
        // 获取 Binding
        val binding = holder.dataBinding
        if (binding != null) {
            binding.bean = item
            binding.executePendingBindings()
        }
    }
}