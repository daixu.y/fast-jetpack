package com.aisier.adapter

import android.view.View
import android.view.ViewGroup
import com.aisier.R
import com.aisier.bean.BannerBean
import com.aisier.databinding.ItemBannerBinding

class BannerBaseAdapter(itemList: MutableList<BannerBean>, brId: Int) : BaseAdapter<BannerBean, ItemBannerBinding>(itemList, brId) {
    override fun getLayoutId(): Int {
        return R.layout.item_banner
    }

    companion object {
        private const val TYPE_HEADER = 1

        private const val TYPE_NORMAL = 0
    }

    var onItemClickListener: OnItemClickListener? = null

    var headerView: View? = null
        set(headerView) {
            field = headerView
            notifyItemInserted(0)
        }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0 && headerView != null) {
            //第一个item应该加载Header
            TYPE_HEADER
        } else {
            TYPE_NORMAL
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == TYPE_HEADER) ViewHolder(headerView!!) else super.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_NORMAL) {
            super.onBindViewHolder(holder, position)
            holder.itemView.setOnClickListener { v -> onItemClickListener?.onItemClick(v, position) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}