package com.aisier.adapter

import com.aisier.architecture.base.BaseAdapter
import com.aisier.bean.BannerBean
import com.aisier.databinding.ItemBannerBinding

class BannerListAdapter(override val layoutId: Int) : BaseAdapter<BannerBean, ItemBannerBinding>() {
    override fun onBindViewHolder(binding: ItemBannerBinding, item: BannerBean) {
        binding.bean = item
    }
}