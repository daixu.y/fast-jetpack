package com.aisier.ui

import android.util.Log
import androidx.activity.viewModels
import com.aisier.R
import com.aisier.architecture.base.BaseActivity
import com.aisier.databinding.ActivitySavedStateBinding
import com.aisier.vm.SavedStateViewModel

class SavedStateActivity : BaseActivity<ActivitySavedStateBinding>() {

    private val stateViewModel by viewModels<SavedStateViewModel>()

    override fun init() {
        Log.i("SavedStateActivity--> ", "SavedStateViewModel: $stateViewModel")
        Log.i("SavedStateActivity--> ", "userName: ${stateViewModel.userName}")
        val value: String = stateViewModel.inputLiveData.value.toString()
        Log.i("SavedStateActivity--> ", "input text: ${value}")

        binding?.submit?.setOnClickListener {
            stateViewModel.userName = "Hello world"
            val inputText: String = binding?.editText?.toString()!!
            stateViewModel.inputLiveData.value = inputText
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_saved_state
    }


}