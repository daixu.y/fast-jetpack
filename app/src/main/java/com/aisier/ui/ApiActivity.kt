package com.aisier.ui

import androidx.activity.viewModels
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.aisier.R
import com.aisier.architecture.base.BaseActivity
import com.aisier.architecture.util.toast
import com.aisier.bean.BannerBean
import com.aisier.bean.Sku
import com.aisier.bean.UpdateInventoryBean
import com.aisier.bean.WxArticleBean
import com.aisier.databinding.ActivityApiBinding
import com.aisier.vm.ApiViewModel
import kotlinx.coroutines.runBlocking

class ApiActivity : BaseActivity<ActivityApiBinding>() {

    private val mViewModel by viewModels<ApiViewModel>()
    private lateinit var adapter: BannerAdapter

    override fun init() {
        initView()
        initData()
        initObserver()
    }

    private fun initView() {
        adapter = BannerAdapter(R.layout.item_banner)
        binding?.recyclerView?.layoutManager = LinearLayoutManager(this)
        binding?.recyclerView?.adapter = adapter

        adapter.setOnItemClickListener { adapter, view, position ->
            val bannerBean: BannerBean = adapter.data[position] as BannerBean
            toast("position= $position image=${bannerBean.image}")
        }

        runBlocking {

        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_api
    }

    private fun initObserver() {
        mViewModel.wxArticleLiveData.observeState(this) {
            onSuccess { data: List<WxArticleBean> ->
                showNetErrorPic(false)
                binding?.data = data.toString()
            }

            onFailed { code, msg ->
            }

            onException {
                showNetErrorPic(true)
            }

            onEmpty {
            }

            onComplete {
                dismissLoading()
            }
        }

        mViewModel.mediatorLiveDataLiveData.observe(this) {
            showNetErrorPic(false)
            binding?.data = it.data.toString()
        }

        mViewModel.userLiveData.observeState(this) {
            onSuccess {
                binding?.data = it.toString()
            }

            onComplete {
                dismissLoading()
            }
        }

        mViewModel.bannerLiveData.observeState(this) {
            onSuccess {
                adapter.setList(it)
            }

            onComplete {
                dismissLoading()
            }
        }

        mViewModel.updateInventoryLiveData.observeState(this) {
            onSuccess {
                binding?.data = it
            }

            onEmpty { msg ->
                binding?.data = "msg$msg"
            }

            onComplete {
                dismissLoading()
            }
        }
    }

    private fun showNetErrorPic(isShowError: Boolean) {
        binding?.tvContent?.isGone = isShowError
        binding?.ivContent?.isVisible = isShowError
    }

    private fun initData() {
        supportFragmentManager.beginTransaction().replace(R.id.fl_contain, ApiFragment()).commit()
        binding?.btnNet?.setOnClickListener {
            showLoading()
            mViewModel.requestNet()
        }
        binding?.btnNetError1?.setOnClickListener {
            showLoading()
            mViewModel.requestNetError()
        }
        binding?.btnMultipleDataSourcesDb?.setOnClickListener {
            binding?.data = ""
            mViewModel.requestFromDb()
        }
        binding?.btnMultipleDataSourcesNet?.setOnClickListener {
            binding?.data = ""
            mViewModel.requestFromNet()
        }
        binding?.btLogin?.setOnClickListener {
            showLoading()
            mViewModel.login("FastJetpack", "FastJetpack")
        }
        binding?.btBanner?.setOnClickListener {
            showLoading()
            mViewModel.fetchBanner("1")
        }
        binding?.btUpdate?.setOnClickListener {
            showLoading()

            val skus = arrayListOf<Sku>()
            val sku = Sku("99", "2491")
            val sku2 = Sku("199", "2492")
            skus.add(sku)
            skus.add(sku2)
            val bean = UpdateInventoryBean("520", skus)

            mViewModel.updateInventory(bean)
        }
    }

}