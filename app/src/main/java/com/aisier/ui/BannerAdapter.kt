package com.aisier.ui

import com.aisier.bean.BannerBean
import com.aisier.databinding.ItemBannerBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder

class BannerAdapter(layoutResId: Int) :
    BaseQuickAdapter<BannerBean, BaseDataBindingHolder<ItemBannerBinding>>(layoutResId) {

    override fun convert(holder: BaseDataBindingHolder<ItemBannerBinding>, item: BannerBean) {
        val binding = holder.dataBinding
        binding?.bean = item
        binding?.executePendingBindings()
    }
}