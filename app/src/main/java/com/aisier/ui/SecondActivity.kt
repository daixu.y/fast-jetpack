package com.aisier.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.aisier.R
import com.aisier.architecture.base.BaseActivity
import com.aisier.architecture.util.toast
import com.aisier.databinding.ActivitySecondBinding
import com.aisier.livedata.RequestPermissionLiveData
import com.aisier.livedata.TakePhotoLiveData
import com.aisier.livedata.TimerGlobalLiveData

class SecondActivity : BaseActivity<ActivitySecondBinding>() {

    private var takePhotoLiveData: TakePhotoLiveData =
        TakePhotoLiveData(activityResultRegistry, "key")

    private var requestPermissionLiveData = RequestPermissionLiveData(activityResultRegistry, "key")

    private val requestPermissionLauncher: ActivityResultLauncher<String> =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { result: Boolean ->
            toast("request permission $result")
        }

    override fun init() {

        takePhotoLiveData.observe(this) { bitmap ->
            binding?.imageView?.setImageBitmap(bitmap)
        }

        binding?.btTakePhoto?.setOnClickListener {
            takePhotoLiveData.takePhoto()
        }

        binding?.btStopTimer?.setOnClickListener {
            TimerGlobalLiveData.get().cancelTimer()
        }

        TimerGlobalLiveData.get().observe(this) {
            Log.i(TAG, "GlobalTimer value: == $it")
        }

        binding?.btRequestPermission?.setOnClickListener {
            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }

        binding?.btRequestPermissionV2?.setOnClickListener {
            requestPermissionLiveData.requestPermission(Manifest.permission.RECORD_AUDIO)
        }

        requestPermissionLiveData.observe(this) {
            toast("权限RECORD_AUDIO请求结果   $it")
        }

        binding?.btBack?.setOnClickListener {
            setResult(Activity.RESULT_OK, Intent().putExtra("key", "返回消息"))
            finish()
        }
    }

    companion object {
        private const val TAG = "SecondActivity-->"
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_second
    }
}