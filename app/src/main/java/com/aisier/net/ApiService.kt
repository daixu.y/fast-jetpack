package com.aisier.net

import com.aisier.bean.BannerBean
import com.aisier.bean.UpdateInventoryBean
import com.aisier.bean.User
import com.aisier.bean.WxArticleBean
import com.aisier.network.entity.ApiResponse
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiService {

    @GET("wxarticle/chapters/json")
    suspend fun getWxArticle(): ApiResponse<List<WxArticleBean>>

    @GET("abc/chapters/json")
    suspend fun getWxArticleError(): ApiResponse<List<WxArticleBean>>

    @FormUrlEncoded
    @POST("user/login")
    suspend fun login(@Field("username") userName: String, @Field("password") passWord: String): ApiResponse<User?>

    @GET("banner/get_banner_list")
    suspend fun getBannerList(@Query("seat_id") seatId: String): ApiResponse<List<BannerBean>>

    @POST("shop_goods/update_inventory")
    suspend fun updateInventory(@Body req: UpdateInventoryBean): ApiResponse<String>

    companion object {
        // const val BASE_URL = "https://wanandroid.com/"
        const val BASE_URL = "https://jss.test.zjjushuashua.com/api/"
    }
}