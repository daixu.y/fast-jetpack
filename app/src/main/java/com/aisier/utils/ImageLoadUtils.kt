package com.aisier.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import com.aisier.R

object ImageLoadUtils {

    @JvmStatic
    @BindingAdapter("bindingAvatar")
    fun bindingAvatar(imageView: ImageView, url: String) {
        imageView.load(url) {
            crossfade(true)
            placeholder(R.mipmap.ic_launcher_round)
        }
    }

    @JvmStatic
    @BindingAdapter("bindSmallImage")
    fun bindingSmallImage(imageView: ImageView, url: String) {
        imageView.load(url) {
            crossfade(true)
            placeholder(R.mipmap.ic_launcher_round)
            size(280,280)
        }
    }

    @JvmStatic
    @BindingAdapter("bindingImage")
    fun bindingImage(imageView: ImageView, url: String) {
        imageView.load(url) {
            crossfade(true)
            placeholder(R.mipmap.ic_launcher_round)
        }
    }
}